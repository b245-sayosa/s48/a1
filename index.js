// console.log("Admin post")

let posts = [];

//post will serve as our mock database.
//array of objects

/*

  {
    id: value,
    title: value,
    body: value
  }

*/ 

let count = 1;
//add post data to the mock database
document.querySelector('#form-add-post').addEventListener("submit", (event) => {
    //preventDefualt() function stops the auto reload of the webpage when submitting.
    event.preventDefault();

    let newPost = {
        id: count,
        title:document.querySelector("#txt-title").value,
        body:document.querySelector("#txt-body").value
    }
    console.log(newPost)
   //we use push mtehiod to add the new post to the mock dtabase
    posts.push(newPost)
    console.log(posts)
  //to have auto increment 
    count++
    showPost(posts)
    
    title:document.querySelector("#txt-title").value = ""
    body:document.querySelector("#txt-body").value = ""
})
//show posts
  const showPost = (posts) => {
    let postEntries = ``;
     
    posts.reverse();
    posts.forEach((post) => {
        postEntries += `
        <div id = "post-${post.id}">
                <h3 id = "post-title-${post.id}">${post.title}</h3>
                <p id = "post-body-${post.id}">${post.body}</p>
                <span>
                <button onclick="editPost(${post.id})">Edit</button>
                </span>
                <span>
                <button onclick="deletePost(${post.id})">Delete</button>
                </span>
                
        </div>`
    })
    document.querySelector("#div-post-entries").innerHTML = postEntries
  }
  const deletePost = (postId) => {
    let indexToRemove;
    posts.forEach((post, index) => {
        if(post.id === postId){
            indexToRemove = index;
            alert(`you deleted post number ${post.id}`)
        }
    });

    posts.splice(indexToRemove, 1)
    showPost(posts);
    
};


 //edit post 
    const editPost = (id) => {
           let title = document.querySelector(`#post-title-${id}`).innerHTML
           let body = document.querySelector(`#post-body-${id}`).innerHTML
           console.log(title)
           console.log(body)

           document.querySelector("#txt-edit-id").value = id;
           document.querySelector("#txt-edit-title").value = title
           document.querySelector("#txt-edit-body").value = body
    }

    document.querySelector("#form-edit-post").addEventListener("submit", (event)=> {
      event.preventDefault();
      //foEach or the check/find the document to be edited

      posts.forEach(post => {
        let idToBeEdited = document.querySelector("#txt-edit-id").value
        console.log(typeof idToBeEdited)
        console.log(typeof post.id)
        if(post.id == idToBeEdited ){
            let title = document.querySelector("#txt-edit-title").value;
            let body = document.querySelector("#txt-edit-body").value;


            posts[post.id-1].title = title;
            posts[post.id-1].body = body;
            alert("Edit is successful!")
            showPost(posts);
            document.querySelector("#txt-edit-id").value = ""
            document.querySelector("#txt-edit-title").value = ""
            document.querySelector("#txt-edit-body").value = ""
        }
      })
    })


   

    